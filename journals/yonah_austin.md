Journal Entry 2/22/23
    Attempted to do the update endpoint. Was a flop. Corrected other errors in accounts queries and routers.

Journal Entry 2/21/23
    Impletemented third party API for spotify. Created query and route for getting the access token for a user.

Journal Entry 2/16/23
    Created the queries and routers for add, updating, and deleting a playlist. Discussed with the instructors how to implement the mood and avatar fields
    in our back-end.

Journal Entry 2/15/23
    Worked with th group to create the authentication for user accounts. Redid migrations
    and yaml files.

Journal Entry 2/14/23
    Built the yaml, requirements.txt, and .gitattributes files. We also created
    our first migration for accounts and playlists models.

Journal Entry 2/13/23
    Showed our wireframes to the instructors and the class. Got some feedback
    and made some adjustments to the wireframe. Decided on the database for the project.
    Finished the week 1 explorations in Learn.

Journal Entry 2/10/23
    Met with an instructor to review our API endpoints and
    added an endpoint for track details and list of user playlists.
    Discussed and created the first issue in GitLab for the project.

Journal Entry 2/9/23
    Collaborated with my group members to create the API designs for the project.
    Added wireframe image from excalidraw to the project file.
