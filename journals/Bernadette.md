2/22/2023
- I drove today! BEEP BEEP BEEP BEEP!
- We thought we were ready to dive into our Spotify endpoints, but we weren't. We needed to fix our Accounts endpoints.
- Our 'get token' endpoint wasn't working, so we reformatted our AccountOut model to work around some hashed password issues.
- We realized we need to add another table, and will be working on that tomorrow. The idea is to use tracks and playlists in a third table as foreign keys, which compiles into a 'users specific playlist' table.
- 

2/21/2023
- Yonah drove all day today! Beep beep! 8 whole hours! Go Yonah!
- It took us forever to figure out how to get Spotify's API to work (something something tokens? something authenticators? swapping?!) BUT WE DID IT.
- Wrote our first successful call to the Spotify API! We can retrieve a track via it's ID, and have narrowed down the response to return the artists name, the album, and the album art.

2/16/2023
- Shakeeb and Ben drove today! Beep beep!
- We continued to set up our endpoints. We can now delete a playlist, get a playlist, update a playlist, and we can list all playlists.

2/15/2023
- I drove today! Beep beep!
- We successfuly got our login/logout/createuser paths to work!
- Added our authenticator and added signing key to the env file
- Added router, queries, and pool files

2/14/2023
- Happy Valentine's Day!
- Got our Dockerfile in order, and managed to create and run all containers (GHI, API, pgAdmin, db)
- Started to tackle Authentication key (signing key)

2/13/2023
- Monday was a day off for me.

2/10/2023
- Friday was a day off for me.

2/9/2023
- Finished Wireframing our project using Excalidraw
- Created and conceptually connected API endpoints for our app
- Wrote our ReadMe.Md
