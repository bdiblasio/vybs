Feb 22
Had a lot of issues with our endpoints, also had issues with authentication that were not expecting. Took a long time to figure out, going to wrap up endpoints tomorrow and move on to frontend by the end of the week.

Feb 21
Ran into alot issues trying to use a third party python library for using spotify, decided to scrap that and use spotify api, getting spotify api to work with our api ate up the whole day. But it finally worked. We initally had issues trying to get our api keys to work, but in the end it worked and we were successful!

Feb 16
Worked on routers and queries, successful got all playlist, update playlist, delete playlist, get playlist.

Feb 15
Got authentication to work, had a few bugs that Riley helped us smash. We were successful in making a user, login and logout.

Feb 14
Got docker containers setup and running, got pgAdmin setup and started working on authentication

Feb 13
Presented the wire frame, Riley suggested to add an additional page for adding songs to playlist.

Feb 10
Need to access SpotifyAPI for getting tracks NOT songs. Track Id will be used for adding songs to the user playlist. Finalized API endpoints after discussing with Riley. Going to go through exploration for fastAPI and start coding on Monday!

Feb 9
Worked on our API endpoints and restructured some of our wireframes to work better. Had an to figure out how to add songs to the user playlist.
Went to Riley for help and got it figured. Solution that we got was, once the user makes a playlist and id is assigned to it. The playlist id is then used for adding songs to said playlist.
