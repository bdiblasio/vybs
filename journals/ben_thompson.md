2/22/23
Lots of troubleshooting getting  out authentication endpoints to function properly. Discussed creating a third table to link playlists and tracks together in order to display a list of tracks. 

2/21/23
After hours of troublshooting we were able to make a get request using spotifys API data to return track information

2/16/23
Created our create playlist, delete playlist, update playlist, list playlists, and get playlists routes and queries. T'was a big day.

2/15/23
Completed authentication and got login, logout, and create user working.

2/14/23
Finished up getting the containers up and running. Started to work on authentication by creating a sign in key and creating out sql tables in our migrations directory.

2/13/23
Finished up FastAPI exploration and began reading up on authentication and database creation

2/10/23
Updated the api endpoints and started to read up on FastAPI

2/9/23
Created our api designs for the wireframe of our web application
