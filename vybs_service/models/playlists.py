from pydantic import BaseModel

class PlaylistIn(BaseModel):
    name: str
    description: str
    mood: str
    account_id: int
    track_id: str | None

class PlaylistOut(BaseModel):
    id: int
    name: str | None
    description: str | None
    mood: str | None
    account_id: int | None
    track_id: str | None
