from pydantic import BaseModel

class SongIn(BaseModel):
    name: str
    artist: str
    song_length: int
    album: str
    album_cover: str
    playlist_id: int

class SongOut(BaseModel):
    id: int | None
    name: str | None
    artist: str | None
    song_length: int | None
    album: str | None
    album_cover: str | None
    playlist_id: int | None
