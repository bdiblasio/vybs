import os
import requests
import base64

def auth_token():
    client_id = os.environ["SPOTIFY_CLIENT_ID"]
    client_secret = os.environ["SPOTIFY_CLIENT_SECRET"]

    auth_str = f"{client_id}:{client_secret}"
    b64_auth_str = base64.b64encode(auth_str.encode('ascii')).decode('ascii')

    auth_options = {
        'url': 'https://accounts.spotify.com/api/token',
        'headers': {
            'Authorization': 'Basic ' + b64_auth_str
        },
        'form': {
            'grant_type': 'client_credentials'
        },
        'json': True
    }

    response = requests.post(**auth_options)

    if response.status_code == 200:
        token = response.json()['access_token']

    return token


SPOTIFY_CLIENT_ID = "e70e1fab5c644e50b62b93b47adba503"
SPOTIFY_CLIENT_SECRET = "46f6444aabca4a0f9cab6ae0ff40e95f"
def get_token():
    client_id = SPOTIFY_CLIENT_ID
    client_secret = SPOTIFY_CLIENT_SECRET
    auth_string = client_id + ":" + client_secret
    auth_bytes = auth_string.encode("ascii")
    auth_b64 = base64.b64encode(auth_bytes).decode("ascii")
    headers = {"Authorization": "Basic " + auth_b64}
    auth_data = {"grant_type": "client_credentials"}
    auth_url = "https://accounts.spotify.com/api/token"
    response = requests.post(auth_url, headers=headers, data=auth_data)
    if response.status_code == 200:
        token = response.json()["access_token"]
        return token
    else:
        return {"error": "Failed to get access token"}
