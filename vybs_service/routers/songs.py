from fastapi import (
    Depends,
    HTTPException,
    status,
    Response,
    APIRouter,
    Request,
)
from jwtdown_fastapi.authentication import Token
from models.songs import SongIn, SongOut
from queries.songs import SongQueries
from pydantic import BaseModel
from authenticator import authenticator
from auth_token import auth_token, get_token
import base64
import requests

router = APIRouter()

@router.get("/tracks/{track_id}")
def get_track(
    track_id: str,
    response: Response,
    repo: SongQueries = Depends(),
):
    result = repo.get_track(track_id)
    if result is None:
        response.status_code = 404
    else:
        return result


@router.post("/playlists/{playlist_id}/tracks")
async def add_track_to_playlist(request:Request, playlist_id:str):
    access_token = get_token()
    payload = await request.json()
    track_id = payload["track_id"]
    url = f"https://api.spotify.com/v1/playlists/{playlist_id}/tracks"
    headers = {"Authorization": f"Bearer{access_token}", "Content-Type": "application"}
    data = {"uris": ["spotify:track:" + track_id]}
    response = requests.post(url, headers=headers, json=data)
    return response.json


# @router.post("/playlists/{playlist_id}/{track_id}")
# def add_track(
#     playlist_id: int,
#     track_id: str,
#     track: SongIn,
#     response: Response,
#     repo: SongQueries = Depends(),
#     # account_data: dict = Depends(authenticator.get_current_account_data),
# ):
#     result = repo.add_track(playlist_id, track_id, track)
#     return result



SPOTIFY_CLIENT_ID = "e70e1fab5c644e50b62b93b47adba503"
SPOTIFY_CLIENT_SECRET = "46f6444aabca4a0f9cab6ae0ff40e95f"
@router.get("/callback")
async def spotify():
    client_id = SPOTIFY_CLIENT_ID
    client_secret = SPOTIFY_CLIENT_SECRET
    client_id = SPOTIFY_CLIENT_ID
    client_secret = SPOTIFY_CLIENT_SECRET
    auth_string = client_id + ":" + client_secret
    auth_bytes = auth_string.encode("ascii")
    auth_b64 = base64.b64encode(auth_bytes).decode("ascii")
    auth_headers = {"Authorization": "Basic " + auth_b64}
    auth_data = {"grant_type": "client_credentials"}
    auth_url = "https://accounts.spotify.com/api/token"
    response = requests.post(auth_url, headers=auth_headers, data=auth_data)
    if response.status_code == 200:
        token = response.json()["access_token"]
        return {"token": token}
    else:
        return {"error": "Failed to get access token"}
