from fastapi import (
    Depends,
    HTTPException,
    status,
    Response,
    APIRouter,
    Request,
)
from jwtdown_fastapi.authentication import Token
from models.track_playlist import Track_playlistIn, Track_playlistOut
from queries.track_playlist import Track_playlistQueries
from pydantic import BaseModel
from authenticator import authenticator
import requests
import json

router = APIRouter()


@router.post("/track_playlist/{track_id}/{playlist_id}/" response_model=Track_playlistOut)
def create_track_playlist(
    track_playlist: Track_playlistIn,
    response: Response,
    repo: Track_playlistQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    # SELECT id from track WHERE track_id = {track_id}
    if track.id is not None:
        

    return repo.create(track_playlist=track_playlist, account_id=account_data['id'])
