import json
import os
from models.songs import SongIn, SongOut
from queries.pool import pool
import requests
from queries.pool import pool
from auth_token import get_token


SPOTIFY_CLIENT_ID = "e70e1fab5c644e50b62b93b47adba503"
SPOTIFY_CLIENT_SECRET = "46f6444aabca4a0f9cab6ae0ff40e95f"
SPOTIFY_REDIRECT_URI = "https://localhost:8000/callback/"


class SongQueries:
    def get_track(self, track_id: str):
        access_token = get_token()
        result = requests.get(
            'https://api.spotify.com/v1/tracks/' + track_id,
            headers = {"Authorization": f"Bearer {access_token}"}
            )
        content = json.loads(result.content)
        output=[]
        artist_name = content['album']['artists'][0]['name']
        images = content['album']['images']
        title = content['album']['name']
        output.extend([artist_name, images, title])
        return output

    # def add_track(self, playlist_id: str):
    #     access_token = get_token()
    #     print(access_token)
    #     result = requests.post(
    #         'https://localhost:8000/playlists/{playlist_id}',
    #         headers = {"Authorization": f"Bearer {access_token}"}
    #         )
    #     print(result)
    #     content = json.loads(result.content)
    #     print("******", content)
    #     return content

    #     # output=[]
    #     # artist_name = content['album']['artists'][0]['name']
    #     # images = content['album']['images']
    #     # title = content['album']['name']
    #     # output.extend([artist_name, images, title])
    #     # return output

    def add_track(
        self, playlist_id: int, track: SongIn,
    ) -> SongOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                result = cur.execute(
                    """
                    INSERT INTO track (name, artist, song_length, album, album_cover, playlist_id, track_id)
                    VALUES (%s,%s,%s,%s,%s,%s,%s)
                    RETURNING id;
                    """,
                    [track.name, track.artist, track.song_length, track.album, track.album_cover, playlist_id, track.track_id]
                )
                id = result.fetchone()[0]
                return SongOut(
                    id=id,
                    name=track.name,
                    artist=track.artist,
                    song_length=track.song_length,
                    album=track.album,
                    album_cover=track.album_cover,
                    playlist_id=playlist_id,
                    track_id=track.track_id
                )
