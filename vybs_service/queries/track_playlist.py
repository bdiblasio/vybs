import os
from models.track_playlist import Track_playlistIn, Track_playlistOut
from queries.pool import pool


class Track_playlistQueries:
    def create(
            self, track_playlist: Track_playlistIn,
            id: int,
        ) -> Track_playlistOut:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        INSERT INTO track_playlist (track_id, playlist_id)
                        VALUES (%s, %s)
                        RETURNING id;
                        """,
                        [track_playlist.track_id, track_playlist.playlist_id]
                    )
                    id = result.fetchone()[0]
                    return Track_playlistOut(
                        id=id,
                        track_id=track_playlist.track_id,
                        playlist_id=track_playlist.playlist_id
                    )
